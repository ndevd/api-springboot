package com.neodev.api.springbootAPI.controller;

import com.neodev.api.springbootAPI.model.Student;
import com.neodev.api.springbootAPI.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private IStudentService estudentServ;

    //create
    @PostMapping("/students/create")
    public String createStudent(@RequestBody Student stu) {
        estudentServ.saveStudent(stu);
        return "Student successfully added";
    }

    //list
    @GetMapping("/students/list")
    public List<Student> getStudents() {
        return estudentServ.getStudents();
    }

    //delete
    @DeleteMapping("/students/delete/{id}")
    public String deleteStudent(@PathVariable Long id) {
        estudentServ.deleteStudent(id);

        return "Student successfully deleted";
    }


}
