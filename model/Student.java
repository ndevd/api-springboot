package com.neodev.api.springbootAPI.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id_student;
    private int age;
    private String name;
    private String last_name;
    @OneToMany(mappedBy = "subject")
    private List<Subject> subjectList;

    public Student() {
    }

    public Student(Long id_student, int age, String name, String last_name, List<Subject> subjectList) {
        this.id_student = id_student;
        this.age = age;
        this.name = name;
        this.last_name = last_name;
        this.subjectList = subjectList;
    }
}
