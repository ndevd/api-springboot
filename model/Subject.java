package com.neodev.api.springbootAPI.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id_subject;
    private String name;
    private String description;
    @ManyToOne
    @JoinColumn(name = "id_student")
    @JsonIgnore
    Subject subject;

    public Subject() {
    }

    public Subject(Long id_subject, String name, String description, Subject subject) {
        this.id_subject = id_subject;
        this.name = name;
        this.description = description;
        this.subject = subject;
    }
}
