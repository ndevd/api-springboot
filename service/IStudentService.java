package com.neodev.api.springbootAPI.service;

import com.neodev.api.springbootAPI.model.Student;

import java.util.List;

public interface IStudentService {

    public List<Student> getStudents();

    public void saveStudent(Student student);

    public void deleteStudent(Long id);

    public Student findStudent(Long id);

}
