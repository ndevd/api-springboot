package com.neodev.api.springbootAPI.service;

import com.neodev.api.springbootAPI.model.Subject;

import java.util.List;

public interface ISubjectService {

    public List<Subject> getSubjects();

    public void saveSubject(Subject subject);

    public void deleteSubject(Long id_subject);

    public Subject findSubject(Long id);
}
