package com.neodev.api.springbootAPI.service;

import com.neodev.api.springbootAPI.model.Student;
import com.neodev.api.springbootAPI.repository.IStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService implements IStudentService {

    @Autowired
    private IStudentRepository studentRepo;

    @Override
    public List<Student> getStudents() {
        List<Student> studentsList = studentRepo.findAll();
        return studentsList;
    }

    @Override
    public void saveStudent(Student student) {
        studentRepo.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepo.deleteById(id);
    }

    @Override
    public Student findStudent(Long id) {
        return studentRepo.findById(id).orElse(null);
    }

}
