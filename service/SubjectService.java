package com.neodev.api.springbootAPI.service;

import com.neodev.api.springbootAPI.model.Subject;
import com.neodev.api.springbootAPI.repository.ISubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService implements ISubjectService {

    @Autowired ISubjectRepository subjectRepo;

    @Override
    public List<Subject> getSubjects() {
        List<Subject> subjectsList = subjectRepo.findAll();
        return subjectsList;
    }

    @Override
    public void saveSubject(Subject subject) {

    }

    @Override
    public void deleteSubject(Long id_subject) {

    }

    @Override
    public Subject findSubject(Long id) {
        return null;
    }
}
